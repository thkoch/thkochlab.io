.PHONY: apt-get-dependencies css watch-css docker-build docker-watch clean


apt-get-dependencies:
	sudo apt-get install entr sassc

css:
	sassc --sourcemap --load-path scss scss/default.scss css/default.css

watch-css:
	while true; do ls -1 scss/* | entr -d make css; done

docker-build:
	docker run --volume=$$(pwd):/build \
		--user $$(id -u):$$(id -g) \
		--workdir=/build \
		--tmpfs=/.cabal \
		thk/hakyll-debian-testing cabal new-run site build

docker-watch:
	docker run --volume=$$(pwd):/build \
		--user $$(id -u):$$(id -g) \
		--workdir=/build \
		--tmpfs=/.cabal \
		thk/hakyll-debian-testing cabal new-run -- site watch --no-server

clean:
	rm -rf _cache public dist-newstyle
