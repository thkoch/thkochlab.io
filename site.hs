{-# LANGUAGE OverloadedStrings #-}

import           Data.Monoid (mappend)
import           Hakyll


config :: Configuration
config = defaultConfiguration {
    destinationDirectory = "public"
}

myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
    { feedTitle       = "Thomas Koch"
    , feedDescription = "personal blog"
    , feedAuthorName  = "Thomas Koch"
    , feedAuthorEmail = "thomas+blog@koch.ro"
    , feedRoot        = "https://blog.koch.ro"
    }


main :: IO ()
main = hakyllWith config $ do
    match (
           "images/**"
      .||. "well-known/**"
      .||. "js/**"
      .||. "oldblog/**"
      .||. "css/*"
      ) $ do
        route   idRoute
        compile copyFileCompiler

    -- build up tags
    tags <- buildTags "posts/*" (fromCapture "tags/*.html")
    let postCtx = tagCloudField "tagcloud" 80.0 200.0 tags
                  `mappend` (postCtxWithTags tags)

    match (fromList ["about.md", "contact.md"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    tagsRules tags $ \tag pattern -> do
        let title = "Posts tagged \"" ++ tag ++ "\""
        let fConf = myFeedConfiguration {
            feedTitle = feedTitle myFeedConfiguration ++ " – " ++ title
        }
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll pattern
            let ctx = constField "title" title
                      `mappend` constField "tag" tag
                      `mappend` listField "posts" postCtx (return posts)
                      `mappend` postCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/tag.html" ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls
        -- learned from https://github.com/nagisa/kazlauskas.me/blob/06d4c3f40b2fa3cf090557631972684f82e9869d/src/Kazlauskas.hs
        version "atom" $ do
            route $ setExtension "atom.xml"
            compile $ do
                posts <- recentFirst =<< loadAllSnapshots pattern "content"
                renderAtom fConf feedContext posts

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html" postCtx
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    create ["feed.xml"] $ do
        route idRoute
        compile $ do
            let feedCtx = postCtx `mappend` bodyField "description"
            posts <- fmap (take 10) . recentFirst =<<
                loadAllSnapshots "posts/*" "content"
            renderAtom myFeedConfiguration feedCtx posts

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    postCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls


    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Home"                `mappend`
                    postCtx

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
feedContext :: Context String
feedContext = mconcat [ bodyField "description"
                      , modificationTimeField "updated" "%Y-%m-%dT%TZ"
                      , defaultContext
                      ]

setItemIdVersion :: Maybe String -> Item a -> Item a
setItemIdVersion version (Item identifier body) =
    Item (setVersion version identifier) body
-- And… a convenience function
setItemsIdVersions v = map (setItemIdVersion v)

postCtxWithTags :: Tags -> Context String
postCtxWithTags tags =
    dateField "date" "%B %e, %Y" `mappend`
    tagsField "tags" tags `mappend`
    defaultContext
