---
title: My exclusion from the Debian project
---

As [[https://lists.debian.org/debian-vote/2023/03/msg00007.html][requested]], I
lay out what happened from my point of view.

#+BEGIN_SRC
Date: Sun, 15 Nov 2020 10:01:53 +0100 (CET)
From: Thomas Koch
To: debian-private@lists.debian.org
Subject: Basic information about Corona in German
#+END_SRC

#+BEGIN_QUOTE
TL;DR: If you're not concerned about the worldwide restrictions to basic rights implemented to combat Covid-19, you can stop reading now.

I wrote a summary about this so called pandemic in German language: http://corona.koch.ro

Never before have I been so concerned in my life. Please have an open mind and take care what you believe.

Otherwise, sorry for the noise.
#+END_QUOTE

#+BEGIN_SRC
Date: Sun, 27 Feb 2022 13:31:59 +0200 (EET)
From: Thomas Koch
To: debian-private@lists.debian.org
Subject: transparency and accountability of DAM work?
#+END_SRC

#+BEGIN_QUOTE

BCC: da-managers@ (sorry for double-post)

Dear fellow Debian members,

yesterday I reactivated my blog and re-added it to planet-debian. My first post was about the things that I am starting to work on right now and the motivation for it.

My blog has been removed afterwards from planet-debian and I received this message:

"""
Hello Thomas,

Back in November 2020, we warned you that Debian is not a platform for
COVID19-related (or any) conspiracy theories, and asked you to please
keep them out of Debian.

Today you added your blog to planet.debian.org, and then posted
https://blog.koch.ro/posts/2022-02-26-corona-plandemic.html   to it.

Are you able to, and willing to commit to, keep your Debian involvement
disconnected from your Corona activism?

For DAM:
<REDACTED>
"""

I believe that the DAMs carry a lot of responsibility and therefor should be thanked a lot. They also have the power as can be seen in this case, to censor voices. I don't deny that there should be oversight about what should and shouldn't be written on any debian owned platform.

However, to my knowledge there is no way for regular DDs to follow what actions DAMs take and for what reason. (Of course one could watch the git history of planet-debian...) This seems problematic. How many other DDs have been warned not to discuss Corona or any other controversial topic? Am I really the only one?

May I suggest, that DAMs consider ways to be transparent and accountable, e.g. by a DD-only mailing list where such serious actions and their justifications are logged? After all, DAMs receive their mandate from DDs. (Of course this means additional overhead. I'd volunteer to help but this is of course silly in the current circumstances.)

To reply to the message sent to me:

I don't intend to write anything further about corona on any Debian platform or list after this mail. I will also not write any additional mail to debian-private@ in this thread. History will (soon) show who was right.

I removed the "debian" tag from the indicated blog post and would like to re-add the debian-tag feed to planet-debian.

I already started looking into the distributed search engine YaCy.net (RFP #768171) and consider packaging it so that it could end up on freedombox. I'd like to post about stuff that I learn along the way.

Thomas
#+END_QUOTE

#+BEGIN_SRC
Date: Wed, 23 Mar 2022 10:51:56 +0200 (EET)
From: Thomas Koch <thomas@koch.ro>
To: debian-private@lists.debian.org, REDACTED (individuals)
Subject: Re: Covid restrictions in Germany?
#+END_SRC

#+BEGIN_QUOTE
Hi REDACTED,

please don't get fooled to believe that Novavax would be harmless!

Dr. Wodarg explains the problems with Novavax here starting from 3:29:00:
https://odysee.com/@Corona-Investigative-Committee:5/Sitzung-77-eng:5

Of course Wodarg is a conspiracy theorist has the wrong friends and thus it's no use to listen to him!

The slides are here: https://www.wodarg.com/impfen/

TL;DR: Novavax also contains Spike-Proteins and these proteins are what cause the many side effects.

So much on that topic on debian-private@. I'm happy to talk more in private.

I just hope that we meet again healthy! I remember our nice conversations in Heidelberg in 2015.
#+END_QUOTE

#+BEGIN_SRC
Date: Sun, 3 Apr 2022 13:05:32 +0300 (EEST)
From: Thomas Koch
To: REDACTED (individuals)
Cc: debian-private@lists.debian.org
Subject: DPL candidate question: geographical diversity
#+END_SRC

#+BEGIN_QUOTE
Dear DPL candidates,

to my knowledge, most Debian project members come from the US and western Europe. Do you have any ideas, plans or motivation to make Debian more geographically universal?

I thought about this question when I read about a ban of foreign Software in Russia and the tiny(?) number of Russian DDs, but the question is of course not limited to current events or one country:

- Putin approves measures to ensure security of Russia=E2=80=99s critical information infrastructure https://tass.com/politics/1429939
- Russia bans imports of software for critical information infrastructure without approval https://tass.com/economy/1429887

Thank you for your candidacy!

Thomas
#+END_QUOTE

#+BEGIN_SRC
Date: Fri, 12 Aug 2022 10:07:57 +0300 (EEST)
From: Thomas Koch
To: "debian-private@lists.debian.org" <debian-private@lists.debian.org>
Cc: REDACTED (individuals)
Subject: neutrality of publicity
#+END_SRC

#+BEGIN_QUOTE
"But remember that the COVID-19 pandemic is not over yet, so take all necessary measures to protect attendees." - https://bits.debian.org/2022/08/debianday2022-call-for-celebration.html

"Team members must word articles in a way that reflects the Debian project's stance on issues rather than their own personal opinion." - (Updating the Debian Publicity Team delegation)
https://lists.debian.org/debian-devel-announce/2018/05/msg00004.html

Please don't comment on hot political topics like Covid-19, Ukraine, Taiwan, Kosovo, etc. on official Debian channels. I continue to try to do the same.

Thank you for your work for Debian!

Team members taken from:
https://wiki.debian.org/Teams/Publicity#DPL-delegated_members
#+END_QUOTE

#+BEGIN_SRC
From: REDACTED <da-manager@debian.org>
To: debian-private@lists.debian.org
Subject: Debian membership of Thomas Koch
Date: Mon, 15 Aug 2022 16:02:15 +0200
#+END_SRC

#+BEGIN_QUOTE
Hello everyone,

Today we revoked Thomas Koch's official Debian Membership.

A timeline of events:

  - In November 2020 Thomas posted about "this so-called pandemic" to
    debian-private@l.d.o

  - DAM issued a warning on the 15th November 2020, clearly telling him
    that Debian is not a platform for spreading conspiracy theories and
    to please keep them out of Debian.

  - In February 2022 he added his blog to Planet Debian to have a
    "Corona Plandemic" post appear.

  - His blog was removed from Planet on the same day (added later again,
    with the condition to *not* have any further corona posts appear on
    Planet Debian).

  - DAM sent another mail asking to keep this off Debian; Thomas
    committed to "not write anything further about corona on any Debian
    platform or list after this mail".

  - In March 2022 he sent mail to REDACTED and debian-private
    about Novavax and spike proteins and so breaking his earlier
    commitment.

  - In August 2022 he flamed on debian-private against publicity team's
    standard COVID-19 warning in a post about organising parties.

We don't mean to make COVID-19 off-topic in Debian: there are lots of
ways in which it affects the project, which is important to be able to
discuss. But one should not use Debian as a platform to amplify
conspiracy theories. In this specific case, one is expected to take
repeated feedback into account.

--
Greetings,
 REDACTED, for the DAMs
#+END_QUOTE
