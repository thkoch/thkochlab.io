---
title: lsp-java coming to debian
tags: debian
---

The [Language Server
Protocol](https://microsoft.github.io/language-server-protocol/) (LSP)
standardizes communication between editors and so called language servers for
different programming languages. This reduces the old problem that every
editor had to implement many different plugins for all different programming
languages. With LSP an editor just needs to talk LSP and can immediately
provide typicall IDE features.

I already packaged the Emacs packages lsp-mode and lsp-haskell for Debian
bullseye. Now lsp-java is waiting in the [NEW
queue](https://ftp-master.debian.org/new.html).

I'm always worried about downloading and executing binaries from random places
of the internet. It should be a matter of hygiene to only run binaries from
official Debian repositories. Unfortunately this is not feasible when
programming and many people don't see a problem with running multiple curl-sh
pipes to set up their programming environment.

I prefer to do such stuff only in virtual machines. With Emacs and LSP I can
finally have a lightweight textmode programming environment even for Java.

Unfortunately the lsp-java mode does [not yet work over
tramp](https://github.com/emacs-lsp/lsp-java/issues/384). Once this is solved,
I could run emacs on my host and only isolate the code and language server
inside the VM.

The next step would be to also keep the code on the host and mount it with
[Virtio FS](https://virtio-fs.gitlab.io) in the VM. But so far the necessary
daemon is not yet in Debian (RFP:
[#1007152](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007152)).

In Detail I uploaded these packages:

- emacs-request ITP [#1007113](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007113)
- bui.el ITP [#1007153](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007153)
- emacs-posframe ITP [#1007158](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007158)
- lsp-treemacs ITP [#1007119](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007119)
- dap-mode ITP [#1007160](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007160)
- emacs-lsp-java ITP [#1007162](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007162)
