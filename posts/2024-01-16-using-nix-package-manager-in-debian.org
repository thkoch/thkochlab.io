---
title: Using nix package manager in Debian
tags: debian, free software, nix, life
---

The [[https://nixos.org][nix]] package manager is [[https://tracker.debian.org/pkg/nix][available in Debian]] since [[https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=877019][May 2020]]. Why would one
use it in Debian?

- learn about nix
- install software that might not be available in Debian
- install software without root access
- declare software necessary for a user's environment inside ~$HOME/.config~

Especially the last point nagged me every time I set up a new Debian
installation. My emacs configuration and my Desktop setup expects certain
software to be installed.

Please be aware that I'm a beginner with nix and that my config might not
follow best practice. Additionally many nix users are already using the new
flakes feature of nix that I'm still learning about.

So I've got this file at ~.config/nixpkgs/config.nix~[fn:confignixpkgs]:

#+BEGIN_SRC nix
with (import <nixpkgs> {});
{
  packageOverrides = pkgs: with pkgs; {
    thk-emacsWithPackages = (pkgs.emacsPackagesFor emacs-gtk).emacsWithPackages (
      epkgs:
      (with epkgs.elpaPackages; [
        ace-window
        company
        org
        use-package
      ]) ++ (with epkgs.melpaPackages; [
        editorconfig
        flycheck
        haskell-mode
        magit
        nix-mode
        paredit
        rainbow-delimiters
        treemacs
        visual-fill-column
        yasnippet-snippets
      ]) ++ [    # From main packages set
      ]
    );

    userPackages = buildEnv {
      extraOutputsToInstall = [ "doc" "info" "man" ];
      name = "user-packages";
      paths = [
        ghc
        git
        (pkgs.haskell-language-server.override { supportedGhcVersions = [ "94" ]; })
        nix
        stack
        thk-emacsWithPackages
        tmux
        vcsh
        virtiofsd
      ];
    };
  };
}
#+END_SRC

Every time I change the file or want to receive updates, I do:

#+BEGIN_SRC sh
nix-env --install --attr nixpkgs.userPackages --remove-all
#+END_SRC

You can see that I install nix with nix. This gives me a newer version than
the one available in Debian stable. However, the nix-daemon still runs as the
older binary from Debian. My dirty hack is to put this override in
~/etc/systemd/system/nix-daemon.service.d/override.conf~:

#+BEGIN_SRC ini
[Service]
ExecStart=
ExecStart=@/home/thk/.local/state/nix/profile/bin/nix-daemon nix-daemon --daemon
#+END_SRC

I'm not too interested in a cleaner way since I hope to fully migrate to Nix
anyways.

[fn:confignixpkgs] Note the ~nixpkgs~ in the path. This is not a config file
for ~nix~ the package manager but for the [[https://github.com/NixOS/nixpkgs][nix package collection]]. See the
[[https://nixos.org/manual/nixpkgs/stable/#chap-packageconfig][nixpkgs manual]].
