---
title: Waiting for a STATE folder in the XDG basedir spec
tags: debian, free software
---
The [XDG Basedirectory
specification](https://wiki.debian.org/XDGBaseDirectorySpecification) proposes
default homedir folders for the categories DATA (~/.local/share), CONFIG
(~/.config) and CACHE (~/.cache). One category however is missing:
[STATE](https://wiki.debian.org/XDGBaseDirectorySpecification#state").  This
category has been requested several times but nothing happened.

Examples for state data are:

- history files of shells, repls, anything that uses libreadline
- logfiles
- state of application windows on exit
- recently opened files
- last time application was run
- emacs: bookmarks, ido last directories, backups, auto-save files,
  auto-save-list

The missing STATE category is especially annoying if you're managing your
dotfiles with a VCS (e.g. via [VCSH](https://github.com/RichiH/vcsh)) and you
care to keep your homedir tidy.

If you're as annoyed as me about the missing STATE category, please voice your
opinion on the <a href="">[XDG mailing
list](http://lists.freedesktop.org/mailman/listinfo/xdg).

Of course it's a very long way until applications really use such a STATE
directory. But without a common standard it will never happen.
