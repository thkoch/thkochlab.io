---
title: shared infrastructure coop
tags: debian, free software
---

I'm working in a very small web agency with 4 employees, one of them part time
and our boss who doesn't do programming. It shouldn't come as a surprise, that
our development infrastructure is not perfect. We have many ideas and dreams
how we could improve it, but not the time.  Now we have two obvious choices:
Either we just do nothing or we buy services from specialized vendors like
github, atlassian, travis-ci, heroku, google and others.

Doing nothing does not work for me. But just buying all this stuff doesn't
please me either. We'd depend on proprietary software, lock-in effects or
one-size-fits-all offerings.  Another option would be to find other small web
shops like us, form a cooperative and share essential services. There are
thousands of web shops in the same situation like us and we all need the same
things:

- public and private Git hosting
- continuous integration (Jenkins)
- code review (Gerrit)
- file sharing (e.g. git-annex + webdav)
- wiki
- issue tracking
- virtual windows systems for Internet Explorer testing
- MySQL / Postgres databases
- PaaS for PHP, Python, Ruby, Java
- staging environment
- Mails, Mailing Lists
- simple calendar, CRM
- monitoring

As I said, all of the above is available as commercial offerings. But I'd prefer the following to be satisfied:

- The infrastructure itself should be open (but not free of charge), like the
  OpenStack Project Infrastructure as presented at LCA. I especially like how
  they review their puppet config with Gerrit.

- The process to become an admin for the infrastructure should work much the
  same like the process to become a Debian Developer. I'd also like the same
  attitude towards quality as present in Debian.

Does something like that already exists? There already is the German
cooperative hostsharing which is kind of similar but does provide mainly
hosting, not services. But I'll ask them next after writing this blog post.

Is your company interested in joining such an effort? Does it sound silly?

Comments:

Sounds promising. I already answered by mail.
Dirk Deimeke (Homepage) am 16.02.2014 08:16
Homepage: http://d5e.org

I'm sorry for accidentily removing a comment that linked to https://mayfirst.org while moderating comments. I'm really looking forward to another blogging engine...
Thomas Koch am 16.02.2014 12:20

Why? What are you missing? I am using s9y for 9 years now.
Dirk Deimeke (Homepage) am 16.02.2014 12:57