---
title: Corona Plandemic
tags: life, peace
---

This is a short statement on what I understand about Covid-19 and the current
situation so that I can refer to it in other posts and don't need to repeat
myself. What I do, think and eventually write is heavily influenced by this
understanding, as outlined futher below.

Also I want to set a counterpoint to the people that still publicly proclaim
the official narrative.

On November 15th 2020 I already wrote an email to debian-private@ with the
subject "Basic information about Corona in German" that referred to my
[information collection on that topic](http://corona.koch.ro). Since then my
understanding has refined but not fundamentally changed:

- The western capitalistic system has finally reached end-of-life in 2019
  after it almost collapsed already in 2008.
- The western oligarchs as organised in the World Economic Forum were well
  aware of this and planned a "Great Reset" of the system through a medical
  emergency. The goal is to create a new system where the old elite will
  remain in power.
- There has been no pandemic in 2020 but only a PCR-test that created
  "cases". Age-normalized mortality in most countries was unspectacular in
  2020\.
- The so called Covid-19 "vaccines" are extremely dangerous in some batches
  with harmless batches in between to not raise suspicion: [How Bad is My
  Batch](https://howbadismybatch.com).

I'm happy to talk in detail about this topic and provide pointers. Right now a
comprehensive overview is available at
[Grand-Jury.net](https://grand-jury.net).

Why is this relevant?

The internet in its current state is an instrument of control and
suppression. This becomes especially obvious in the increase in censorship and
the introduction of vaccine-passports which are planned to become universal
passports for all aspects of our life.

Therefor I want to work on initiatives for a free internet for the
people. Right now I think about the [freedombox](https://freedombox.org),
[distributed search
engines](https://wiki.p2pfoundation.net/Distributed_Search_Engines),
re-decentralization in general and especially a decentralized alternative to
Wikipedia.

