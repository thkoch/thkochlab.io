---
title: Russia is beautiful
tags: life, peace
---

I have been procrastinating this blog post about our recent trip to Russia for
over six weeks now. But today I listened to a report about the Leningrad
Siege[^dlf] from the German State-radio "Deutschlandfunk" and felt deeply
ashamed.

[^dlf]: "Zweiter Weltkrieg - Als die Blockade von Leningrad begann" by Robert
    Baag; link omitted on purpose.

Every German school child learns at least something about the crimes Germans
committed against Jews. But to my knowledge the crimes committed against
Russians are hardly ever mentioned in German Schools. Maybe the most
horrifying crime was the Leningrad Siege. But the above radio broadcast
managed to talk only shortly about the guilt of the Germans and spent the rest
of the time blaming Sowjet leadership for not having evacuated the town
earlier or later leadership for their historiography of the Siege.

I am not a historian and can not say what the truth is here. But it is a
shame. Why did the report not mention the destruction of cultural heritage by
the German army? We visited the beautiful Peterhof next to
St. Petersburg. There is a small exhibition in the park that shows pictures of
what remained of this wonderful palace after the Germans were gone.

Western media is very noisy to mourn the loss of cultural heritage the Buddha
statues of Bamiyan, looting in the Egyptian museum or destruction in
Palmyra. But who in Germany remembers that we haven't been any better just a
few decades ago?

So dishonesty like this enraged me already since some time already when my
wife and I decided to spend our vacation in Russia.

It was interesting to note the reactions of friends and family when we told
them about our plans. The prejudices generally were:

- Moscow is loud, polluted, gray, boring and drear.
- Criminality and Corruption are high.
- Russian kitchen is meat heavy, heavy and monotonous.
- Russian woman are generally very beautiful.

Let me say that I could only find evidence for the latter.

We experienced Russians as both friendly and unfriendly. We were baffled by
the unfriendliness of Russians when they work at a cash point or
cloakroom. However on the other hand I like that they don't seem to be afraid
of losing their jobs.

But more often we had positive encounters. Russians move in the metro so that
we can sit together or young man offer their place for Ioana. When we waited
one hour in line to enter the Eremitage we met a Caucasian family and did not
finish talking when we finally reached the entrance. When we went to a West
Coast Swing party in the evening we were offered a lift to our hotel. When we
searched the [Grand Maket](https://en.wikipedia.org/wiki/Grand_Maket_Rossiya)
museum a mother and a daughter from Moscow addressed us and we went to the
museum together. Later we arranged to meet in Moscow so that they could give
us a tour.

## aggressive Russia?

Western propaganda wants to make us believe that Russia would follow an
aggressive and expansive politic. To my knowledge, Russia has not started any
conflict since the October Revolution. And even before that, Russian expansion
was limited to the scope of gaining access to the Mediterranean Sea.[^1] We
saw many monuments in Moscow and St. Petersburg that remind of the horror of
war. I don't recall that many monuments in Germany that call for peace.

[^1]: To my knowledge, it was the CIA who first sponsored the mujahideen and
    its leader Osama bin Laden in [Operation
    Cyclone](https://en.wikipedia.org/wiki/Operation_Cyclone) and thus forced
    the USSR to get involved in this conflict that happened directly at its
    border.

Most prominently we visited the "Monument to the Heroic Defenders of
Leningrad". I don't like the naming and the heroic style of the monument. But
I can not imagine how a Russian who passes by this monument daily could
support an aggressive politic.

## Moscow is beautiful

The first impression of Moscow is the absence of advertisement. There is no
advertisement at the train station, in the metro or by the street. I would be
interested to learn about why this is. But in any case it is pleasant to the
western eye and it would be wonderful if our cities could benefit from the
same peace.

The streets are extraordinary clean. One hardly finds a graffiti, waste or
even dirt. Everything looks as if somebody just finished cleaning it with
soap. Not only is everything clean but the streets also look newly made and
the sidewalks are made of something that look almost like marble to my
uneducated eye.

If something isn't nice yet, then there is a construction site to make it
nice. I was surprised to see so many renovations going on. This is unusual to
see for me while the whole European Union suffers under an ill guided
austerity dictate imposed by the Germans.[^3]

[^3]: The currently missing investments needed just to maintain the current
    German infrastructure is estimated to be at least 100 billion
    Euros. Public schools alone miss over 30 billion to not degrade further.

Granted, we mostly saw the center of Moscow and it is clear that not all of
the city looks like this. However the center of most German cities I know or
even Zurich can not keep up with Moscow.

And we have not even talked about the parks. We visited the "Exhibition of
Achievements of National Economy" in the evening. I was overwhelmed by the
view of the illuminated fountains, accompanied by music playing from speakers
at every lantern.

![The Friendship of Nations
 fountain](../images/2016-10-17_russia/moscow_vdnkh_fountain.jpg)

I am happy that we also took the time for a short trip to Gorky Park right
before we had to leave. In Germany one would need to pay entrance for such a
park. And even than it would not be so beautiful. Even the toilets in this
public park were remarkably clean. There were play fields for small children,
skateboarding areas for teenagers and an area just to hang out. The latter
even had power plugs so that the teens hanging out there could charge their
phones.[^2]

[^2]: I definitely don't approve how teenagers are made dependent of their
    phones. But I am still impressed by the facilities that are offered for
    free.

We visited the Ostankino Tower in the evening. It was breathtaking to see the
lights of the many million people metropole. I remembered that I read an
article recently about how the US military evaluates the possibilites of using
nuclear bombs against Russia.[^atom] I urge any war strategist to stand 500m
above Moscow and to imagine to press the red button and killing all this
people.

[^atom]: [Bereitet der Westen einen Krieg gegen Russland
    vor?](http://www.nachdenkseiten.de/?p=35031)

![Moscow by night seen from the Ostankino
 tower](../images/2016-10-17_russia/moscow_from_ostankino_night.jpg)

But we also had fun. There was music and Ioana and I danced West Coast Swing
on a glas plate that gave us a look 500m straight down. We maybe hold the
record of having danced West Coast Swing at the highest level of a building?

## lying by omission

We visited the russian political history museum in St. Petersburg. One day was
barely enough for us to visit one of the exhibitions there and we learned a
lot. The most interesting things are those that were omitted in my history
classes at school.

Did you know that almost all western countries invaded Russia after the
October Revolution to fight the Bolsheviks? I only learned this in preparation
of this trip but not at school. Why didn't they leave Russia alone since this
was an inner conflict? Did you know that Napoleon might not have been defeated
if Russians hadn't burned down their own capital? Did you know that the French
Revolution was not about Liberty, Equality, Fraternity but about property?[^4]
Did you know that Stalin offered England and France a partnership against
Hitler and was rejected?[^5] Only afterwards did Stalin agree on the
German–Soviet Non-aggression Pact.

[^4]: [Wikipedia: Declaration of the Rights of Man and of the
    Citizen](https://en.wikipedia.org/wiki/Declaration_of_the_Rights_of_Man_and_of_the_Citizen)

[^5]: (The Telegraph: "Stalin 'planned to send a million troops to stop Hitler
    if Britain and France agreed
    pact'")[http://www.telegraph.co.uk/news/worldnews/europe/russia/3223834/Stalin-planned-to-send-a-million-troops-to-stop-Hitler-if-Britain-and-France-agreed-pact.html]
    18 Oct 2008

## other observations

- We saw a scene in a Russian film in which two men and one woman visited
another man. At the leave-taking the host shacked hands with the two man but
not with the woman. I have been told that it wasn't until long ago that it was
improper for men to shake hands with women that weren't close friends.

- Many things in Russia are still done manually. It might not be bad that this
  jobs exist instead of being displaced by machines:

  - People watching the moving stairs in the metro to stop them in
    emergencies.

  - Many more waiters in restaurants then necessary. (However not necessarily
    better service...) Many restaurants even have dedicated receptionists.

  - Many more cleaning staff in the city, in parks or at toilets than in the
    west.

  - Ticket vendors in the metro instead of only vending machines.

- We were surprised that so few people speak English and lucky that Ioana
  learned enough Russian to solve everyday problems.

- If you want good and fast food in Russia try the Teremok chain! The pancakes
  are delicious!

- Russians appear to read a lot. And they read serious literature. You see a
  lot of Chekhov, Dostoevsky and Tolstoy in the metro. Our friends from Moscow
  explained us that the Russian school curriculum includes a lot of literature
  reading and mother and daughter had an intense discussion whether this would
  actually be a good thing or whether school should rather concentrate on
  stuff that helps to get a job.

- We saw a small demonstration in memory of the 1991 Soviet coup d'état
  attempt.[^6] There are a not a few Russians who think that it would have
  been better for their country had the attempt been successful.

[^6]: [Wikipedia: 1991 Soviet coup d'état
    attempt](https://en.wikipedia.org/wiki/1991_Soviet_coup_d%27%C3%A9tat_attempt)

- I drank tap water in Moscow and St. Petersburg daily and liked it.

- The Grand Maket in St. Petersburg is interesting. But unlike the Miniatur
  Wunderland in Hamburg it lacks almost all humor. Also it is annoyingly
  patriotic.

- The receptionist in our Hotel in Moscow was surprised that we had not
  received a document about our stay in the Hotel in St. Petersburg. He
  informed us that we could be fined when we leave the country if we can not
  provide a complete documentation of where we stayed in Russia. However at
  the airport nobody asked us about this. YMMV. IANAL.

- In Peterhof there are attendants everywhere and they whistle if you dare to
  leave the path or climb a balustrade to make a picture. Just imagine how
  western parents would react if an attendant would dare to whistle because a
  child misbehaves! In the Kremlin you have the honor of being whistled by the
  police instead of just attendants.

- In western cities you see a kind of vandalism called [love
  locks](https://en.wikipedia.org/wiki/Love_lock) that damages bridges. Moscow
  found a nice solution to the problem by providing metalic trees where lovers
  can put their lock and an attendand who takes care that the locks are not
  put anywhere else. We counted more than 10 trees completely covered with
  locks.

![love locks nicely put on a special tree in
 Moscow](../images/2016-10-17_russia/love_locks_moscow.jpg)

I hope we will have the occasion to come back to Russia and that I will
eventually learn Russian.
